# Напишете функция, който приема 1 параметър:
# - лист с два елемента: **цифри** [I, J]
# - ако подадения параметър няма желаната дължина изпишете подходящо съобщение
# - ако стойностите в подадения параметър не отговарят на нужните условия изпишете подходящо съобщение
# - при коректно зададен парамер, функцията трябва да генерира двуизмерен масив, където стойноста на елемент на i-тия ред и j-тата # колона има стойност i * j.

# При начални стойности i=3 и j=5
# Трябва да има изход:

# `[[1, 2, 3, 4, 5], [2, 4, 6, 8, 10], [3, 6, 9, 12, 15]] `
