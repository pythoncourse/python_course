# Напишете функция която приема един параметър text и връща съдържанието му без гласни.
# Решете задачата веднъж с for и веднъж с list comprehention.

# sentence = 'My name is John Doe!'
# print("FOR-loop result: " + remove_vocals_with_for(sentence))
# print("LC result: " + remove_vocals_with_list_comprehension(sentence))

# Result:
# FOR-loop result: My nm s Jhn D!
# LC result: My nm s Jhn D!
