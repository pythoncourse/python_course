import decimal
import json

import boto3

from chalice import Chalice

from snippet_1 import table
from snippet_3 import get_game_records

app = Chalice(app_name='hello', debug=True)

sns_topic = 'cows-and-bulls-game-ready'
sns_topic_arn = 'arn:aws:sns:eu-central-1:129149368053:{}'.format(sns_topic)

sns = boto3.client('sns')

s3 = boto3.client('s3')
S3_BUCKET = 'cows-and-bulls'


def get_game_records(game_id):
    result = table.query(KeyConditionExpression=Key('game_id').eq(game_id))['Items']
    game_record = {
        'play': [],
        'number': result[0]['number']
    }
    play = game_record['play']
    for game in result[1:]:
        play.append({
            'guess': game.get('guess'),
            'result': game.get('result')
        })
    return game_record


class _CustomJSONEncoder(json.JSONEncoder):
    """
    Encode Decimal to int
    """
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return int(o)
        return super(_CustomJSONEncoder, self).default(o)


@app.on_sns_message(topic=sns_topic)
def backup_game(event):
    game_id = event.message
    print('game_id: {}'.format(game_id))
    game_record = get_game_records(game_id)
    print('game_record: {}'.format(game_record))
    return s3.put_object(Bucket=S3_BUCKET, Key='{}.json'.format(game_id), Body=json.dumps(game_record, cls=_CustomJSONEncoder))