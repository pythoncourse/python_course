import random

from boto3.dynamodb.conditions import Key

from snippet_1 import log_game
from snippet_1 import table
from snippet_1 import get_last_guess


def generate_game_id():
    alphabet = list('AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890')
    random.shuffle(alphabet)
    game_id = ''.join(alphabet[:6])
    return game_id


def get_random_number():
    digits = list('0123456789')
    random.shuffle(digits)
    number = ''.join(digits[:4])
    return number


def play():
    game_id = generate_game_id()
    number = get_random_number()
    log_game(game_id=game_id, number=number)
    return {"game_id": game_id, "number": number}


def list_games():
    return table.scan().get('Items')


def check_answer(number, guess):
    cows = 0
    bulls = 0
    for index, digit in enumerate(guess):
        if (digit in number):
            if index == number.index(digit):
                bulls += 1
            else:
                cows += 1
    return {'bulls': bulls, 'cows': cows}


def get_last_guess(game_id):
    return table.query(KeyConditionExpression=Key('game_id').eq(game_id))['Items'][-1]


def guess(data):

    game_id = data['game_id']
    guess = data['guess']

    last_guess = get_last_guess(game_id)

    try_index = last_guess['try_index']
    number = last_guess['number']

    result = check_answer(number, guess)
    log_game(game_id=game_id, number=number, result=result, try_index=try_index, guess=guess)

    return {
        'game_id': game_id,
        'result': result
    }