import json

import boto3

from chalice import Chalice

from snippet_2 import play
from snippet_2 import list_games
from snippet_2 import guess

app = Chalice(app_name='hello', debug=True)

sns_topic = 'cows-and-bulls-game-ready'
sns_topic_arn = 'arn:aws:sns:eu-central-1:129149368053:{}'.format(sns_topic)

sns = boto3.client('sns')


@app.route('/play')
def get_game():
    return play()

@app.route('/')
def get_list_of_games():
    return list_games()

@app.route('/play', methods=['POST'])
def post_a_guess():
    data = json.loads(app.current_request.raw_body.decode('utf-8'))
    result = guess(data)
    if result.get('bulls') == 4:
        sns.publish(TopicArn=sns_topic_arn, Message=result['game_id'])