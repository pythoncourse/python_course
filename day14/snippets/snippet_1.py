import boto3

from boto3.dynamodb.conditions import Key

dynamodb = boto3.resource('dynamodb')

def create_table(table_name):
    table = dynamodb.create_table(
        TableName=table_name,
        KeySchema=[
            {
                'AttributeName': 'game_id',
                'KeyType': 'HASH'
            },
            {
                'AttributeName': 'try_index',
                'KeyType': 'RANGE'
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'game_id',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'try_index',
                'AttributeType': 'N'
            },

        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )
    table.meta.client.get_waiter('table_exists').wait(TableName='cows_and_bulls')
    return table


def get_table(table_name):
    try:
        table = dynamodb.Table(table_name)
        table.item_count
    except:
        table = create_table(table_name)
    return table


def log_game(game_id, try_index=0, **kwargs):
    item = {
        'game_id': game_id,
        'try_index': try_index
        }
    response = table.get_item(Key=item)
    item.update(response.get('Item', {}))
    item['try_index'] += 1
    item.update(kwargs)
    table.put_item(Item = item)
    return {
        'game_id': item['game_id'],
        'try_index': item['try_index']
    }


def list_games():
    return table.scan().get('Items')

table_name = 'cows_and_bulls'
table = get_table(table_name)