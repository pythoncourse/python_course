# Задачата "Съотношение на грешките" да се напише под форма на функция,
# която приема два параметъра:

# - ctrl_str - контролен стринг, който трябва да се провери
# - valid_colors - валидните цветове, със стойност по подразбиране буквите от 'a' до 'm'
# Функцията трябва да връща 'error rate', като стринг във формат "брой грешки/дължина на стринга".
