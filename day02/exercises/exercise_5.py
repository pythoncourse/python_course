# Да се напише функция accum, която приема един параметър.
# Резултатът, който връща, е описан в следните примери:
#   accum("abcd")    # "A-Bb-Ccc-Dddd"
#   accum("RqaEzty") # "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
#   accum("cwAt")    # "C-Ww-Aaa-Tttt"

# Параметърът на accum е string, който може да включва
# само буквите a..z и A..Z