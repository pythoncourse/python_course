#
# Да се напише функция remove(text, what), където:

# text е string
# what е dict

# Ключовете на what са символите, които трябва да се премахнат от text.
# Стойностите на what определят колко срещания на дадения ключ трябва да се премахнат от text.

# Пример:

# text = 'this !is e*xam*p#le!'
# what = {"!": 1,
#         "*": 2,
#         "#": 1}

# Резултата трябва да е:
# this is example!

# Използвайте Test Driven Developmnet за да решите задачата.
# Стъпките, които трябва да се следват са:

# пише се тест
# пуска се теста и той не минава. (ако на този етап теста мине, нещо не е наред)
# git commit (commit message може да описва какъв тест е добавен)
# започва да се пише код, с който един по един тестовете започват да минават.
# на свсеки минал тест се прави commit