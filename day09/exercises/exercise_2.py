# Разширение на Крави и Бикове:

# * Създайте функция, която генерира random стринг от 4 неповтарящи се цифри - generate_number
# * Създайте функция, която при извикването си:
#     1. извиква generare_number
#     2. пита потребителя дали може да познае намисленото число
#     3. проверява дали, предположението на потребителя е резултата от generare_number
#     4. връща информация на потребителя дали е познал или колко крави и бикове има неговото предположение
#     5. Повтаря стъпките от 2. до 4. докато потребителя не познае генерираното число
#
# Hint:
# https://docs.python.org/3/library/random.html
# 
# * Напишете docstring за всяка от функциите 
# * Напишете doctest-тове за generate_number, които проверяват:
#   1. generate_number връща string
#   2. всеки символ в резултата на generate_number е число
#   3. резултата от generate_number е с дължина 4
#   4. резултата от generate_number няма повтарящи се символи

def generare_number():
    """
    Place your docstring here
    """
    # place your code here
    pass