"""
Създайте функция, която приема два елемнта:
initial_string = '''
    {
        "Albert Einstein":  "March 14, 1879",
        "Isaac Newton": "January 4, 1643",
        "Marie Curie": "November 7, 1867"
    }
'''

new_element = {"name": "Charles Darwin", "date": "February 12, 1809"}

И връща резултат:
'''
    {
        "Albert Einstein":  "March 14, 1879",
        "Isaac Newton": "January 4, 1643",
        "Marie Curie": "November 7, 1867",
        "Charles Darwin": "February 12, 1809"
    }
'''

Запишете резултатa във файл.
"""
