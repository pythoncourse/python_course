"""
Създайте функция, която приема два елемнта:
initial_string = '''
    {
        "Albert Einstein":  "March 14, 1879",
        "Isaac Newton": "January 4, 1643",
        "Marie Curie": "November 7, 1867"
    }
'''

new_element = {"name": "Charles Darwin", "date": "February 12, 1809"}

И връща резултат:
'''
    {
        "Albert Einstein":  "March 14, 1879",
        "Isaac Newton": "January 4, 1643",
        "Marie Curie": "November 7, 1867",
        "Charles Darwin": "February 12, 1809"
    }
'''

Запишете резултатa във файл.
"""


initial_string = '''
    {
        "Albert Einstein":  "March 14, 1879",
        "Isaac Newton": "January 4, 1643",
        "Marie Curie": "November 7, 1867"
    }
'''
new_element = {"name": "Charles Darwin", "date": "February 12, 1809"}
file_name = 'workfile.json'



# Solution 1
import json


def add(initial_string, new_element):
    initial_dict = json.loads(initial_string)
    result_dict = initial_dict.copy()
    result_dict.update({new_element['name']: new_element['date']})
    # OR
    # result_dict[new_element['name']] = new_element['date']
    return json.dumps(result_dict, indent=4)


def write_to_file(string_input, file_name="workfile.json"):
    with open(file_name, 'w') as f:
        f.write(string_input)


def add_to_json(input_string, new_element, file_name):
    result_string = add(input_string, new_element)
    write_to_file(result_string, file_name)


# Solution 2

def add_to_json_v2(input_string, new_element, file_name="workfile.json"):
    result_dict = json.loads(input_string)
    result_dict.update({new_element['name']: new_element['date']})
    with open(file_name, 'w') as f:
        json.dump(result_dict, f, indent=4)
    # NOTE: json.dups  !=  json.dump
