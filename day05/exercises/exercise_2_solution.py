"""
Да се напише функция, която приема три параметъра - key, value и dict.
Ако в dict-а няма ключ key, товага той се добавя към dict-а и му се дава стойност value
Ако в dict-а има key или key(n), тогава се създава нов ключ - key(1) или key(n+1) съответно.
n е най-колямото число от всички key(x)

Пример:
key = 'a', value = 1, d = {'b': 1}
d = {'a':1, 'b': 1}

key = 'a', value = 1, d = {'a': 2}
d = {'a(1)': 1, 'a': 2}

key = 'a', value = 1, d = {'a': 0, 'a(2)': 2}
d = {'a': 0, 'a(2)': 2, 'a(3)': 1}

"""

# Example Solution 1


def add(key, value, destination):
    result = destination.copy()
    max_n = 0
    for k in result:
        if k == key and max_n == 0:
            max_n = 1
            continue
        if k.startswith(key+'('):
            n = int(k[k.index('(')+1:k.index(')')])+1
            if n > max_n:
                max_n = n
    if max_n > 0:
        result[f'{key}({max_n})'] = value
        return result
    result[key]=value
    return result

# Example Solution 2
# (Note: Two separate functions doing their own thing. Function add_to_dict is calling max_index where needed.)


def max_index(input_a):
    '''Given a list of strings like aa = ['a', 'a(1)', 'a(5)', 'a(3)']
    return the max index. In this case 5'''
    max_n = 0
    for item in input_a:
        if '(' in item:
            start = item.index('(')
            end = item.index(')')
            n = item[start + 1: end]
            n = int(n)
            if n > max_n:
                max_n = n
    return max_n


def add_to_dict(key, value, d):
    '''Add key: value couple into the provided dict.
    If key already exists (with or without index), apply max index to the new key'''
    result = d.copy()
    matches = [k for k in d if k.startswith(key)]
    if matches:
        max_n = max_index(matches) + 1
        key = key + '(' + str(max_n) + ')'
    result[key] = value
    return result
