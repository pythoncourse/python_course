"""
Да се напише функция, която приема два параметъра dict и ги обединява.
Ако има повтарящи се ключове, да преименува ключа, като добавя (1).

Пример:
d1 = {'a': 1, 'b': 2, 'c': 3}
d2 = {'b': 3, 'e': 4, 'a': 5}

Очакван резултат:
r = {'a': 1, 'b': 2, 'c': 3, 'b(1)': 3, 'e': 4, 'a(1)': 5}
"""


def merge(source, destination):
    result = destination.copy()
    for k, v in source.items():
        if k in result:
            result[k+'(1)'] = v
        else:
            result[k] = v
    return result
