# Съдържание:

* day01/ - Въведение в Python - част 1

# Домашни:

За да предадете домашното за day_n

* Създайте бранч от master наименован day_n_YourName
```bash
git checkout -b day_n_YourName
```
* Напишете вашите решения във файловете от day_n/exercises папката.
* В създадения бранч направете:
```bash
git add day_n/exercises
git commit -m "My amazing solutions to day_n homework"
git push origin  day_n_YourName
```
* Насочете се към: https://bitbucket.org/pythoncourse/python_course/pull-requests/ и създайте Pull Request от day_n_YourName към master
* не забравяйте да се върнете в master 
```bash
git checkout master
```
