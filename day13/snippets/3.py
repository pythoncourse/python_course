from boto3.dynamodb.conditions import Key


def get_game_records(game_id):
    """
    Намиране на всички записи за игра с дадено game_id
    """

    return table.query(KeyConditionExpression=Key('game_id').eq(game_id))['Items']


def load_game(game_id, try_index):
    """
    Намиране на конкретен запис
    """
    return table.get_item(Key={'game_id': game_id, 'try_index': try_index})

# Актуализираме __main__
if __name__ == '__main__':
    #Добавяме редовете
    game = load_game('223', 2)
    games = get_game_records('223')

    print(game)
    print(games)