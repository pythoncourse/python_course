# създаване на таблица
import boto3

dynamodb = boto3.resource('dynamodb')


def create_table(table_name):
    table = dynamodb.create_table(
        TableName=table_name,
        KeySchema=[
            {
                'AttributeName': 'game_id',
                'KeyType': 'HASH'
            },
            {
                'AttributeName': 'try_index',
                'KeyType': 'RANGE'
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'game_id',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'try_index',
                'AttributeType': 'N'
            },

        ],
        ProvisionedThroughput={
            'ReadCapacityUnits': 5,
            'WriteCapacityUnits': 5
        }
    )
    table.meta.client.get_waiter('table_exists').wait(TableName='cows_and_bulls')
    return table


def get_table(table_name):
    try:
        table = dynamodb.Table(table_name)
        table.item_count
    except:
        table = create_table(table_name)
    return table

if __name__=='__main__':
    table_name = 'cows_and_bulls'
    table = get_table(table_name)
