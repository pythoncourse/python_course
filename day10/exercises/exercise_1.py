# Искаме след приключване на една игра (Крави и бикове - exercise_2.py от day09) да имаме информация как е протекла тя.

# За целта подгответе функция logger, която приема за параметри:
#    - предположението на играча
#    - резултата от generated_number

# При всяко извикване функцията трябва:
# - да индентифицира всяка една от цифрите в предположението на играча като крава, бик или нищо
# - да ги запише в подходяща структура например:
# при извикване на logger(guess='6543', number='0567')
# Резултат:
#    {
#            1: {'digit': 6, 'type': 'cow'},
#            2: {'digit': 5, 'type': 'bull'},
#            3: {'digit': 4, 'type': None},
#            4: {'digit': 3, 'type': None},
#    }
#  - да отвори файл log_caw_and_bulls.json да прочете съдържанието му в променлива (list):
# [
#    {
#            '1': {'digit': 6, 'type': 'cow'},
#            '2': {'digit': 5, 'type': 'bull'},
#            '3': {'digit': 4, 'type': null},
#            '4': {'digit': 3, 'type': null}
#    }
# ]
#  - да допълни лог-а с новия елемент и да запази файла
#
# Hints:
#
# Работа с файлове
# отваряне на файла strings.json за писане и записване на s:
#
# s = [] # например dict-а от първия пример по-горе
# with open('strings.json', 'w') as json_data:
#    json.dump(s,json_data)
#
# отваряне на файла strings.json за четене и прочитане на съдържанието му в d:
#
# d = None
# with open('strings.json', 'r') as json_data:
#     d = json.load(json_data)
#
# Прочетете docstring-овете в кода по-долу и изпълнете инструкциите.
#

import json
import unittest

class TestLogger(unittest.TestCase):
    """
    Place your docstring here
    """

    def setUp(self):
        """
        This code will be executed before every test
        Here you should put code that creates log_caw_and_bulls.json file
        and write in it the information that you need.
        Empty list will be good enough.
        """
        # place your code here
        pass

    def tearDown(self):
        """
        This code will be executed after every test
        Here you should put code that removes log_caw_and_bulls.json file
        Or you can remove this method, so you can see what is in the json file between tests.
        """
        # place your code here
        pass
    
    def test_save(self):
        """
        Test if the content of the json file after executing 'logger' function is the expected one.
        """
        # place your code here
        expected_result = "Replace this with what you expected to be in the file"
        actual_result = "Read what is in the file and place it here"
        self.assertEqual(expected_result, actual_result)


def logger(guess, number):
    """
    Place your docstring here
    """
    # place your code here
    pass



if __name__ == '__main__':
    unittest.main()